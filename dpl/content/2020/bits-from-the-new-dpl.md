Title: Bits from the new DPL
Date: 2020-04-21 19:00
Tags: dpl, election, Sruthi Chandran, Brian Gupta, COVID-19
Slug: bits-from-new-dpl-1
Author: Jonathan Carter
Status: published

My fellow Debianites,

It's been one month, one week and one day since I
[decided to run](https://lists.debian.org/debian-vote/2020/03/msg00007.html) for this DPL
term. The Debian community has been through a variety of interesting times during the last decade,
and instead of focusing on grand, sweeping changes for Debian, core to *my*
DPL campaign was to establish a sense of normality and stability so that we can work on
community building, continue to focus on technical excellence and serve our users the best we can.

Thing don't always work out as we plan, and for many of us, Debian recently had
to take a back seat to personal priorities.
Back when I posted my intention to run, there were
[125 260](https://www.who.int/docs/default-source/coronaviruse/situation-reports/20200312-sitrep-52-covid-19.pdf?sfvrsn=e2bfc9c0_4) confirmed cases of
COVID-19 globally. Today, that number is [20 times](https://www.who.int/docs/default-source/coronaviruse/situation-reports/20200421-sitrep-92-covid-19.pdf) higher, with the
*actual* infected number likely to be significantly higher. A large number
of us are under lock-down, where we not only fear the disease and its effect on
local hospitals and how it will affect our loved ones, but also our very livelihoods
and the future of our local businesses and industry.

I don't mean to be gloomy with the statement above, I am after all, an optimist -
but unfortunately it does get even worse. Governments and corporations
around the world have started to take advantage of COVID-19 in negative ways and
are making large sweeping changes that undermine the privacy and rights of individuals
everywhere.

For many reasons, including those above, I believe that the Debian project
is more important and relevant now than it's ever been before. The world needs a
free, general purpose operating system, unburdened by the needs of profit,
which puts the needs of its users first, providing a safe and secure platform for
the computing needs of the masses.

While we can't control or fix all the problems in the world, we can control our
response to it, and be part of the solutions that bring the change we want to
see.

During my term as DPL, I will be available to help with problems in our community
to the maximum extent that my time permits. If we help ourselves, we will be in
a better position to help others. If you (or your team) get stuck and are in
need of help, then please do not hessitate to <a href="mailto:leader@debian.org">e-mail me</a>.

## A few thank-yous

As incoming DPL, I'd like to thank Sam Hartman on behalf of the project
for the work that he's done over the last year as DPL. It's a tremendous
time commitment that requires constant attention to detail. On Sunday,
Sam and I had a handover meeting where we discussed various DPL
responsibilities including finances, delegations (including specifics of
some delegations), legal matters, outreach and other questions I had.
I'd also like to thank Sam for taking the time to do this.

Thank you to Sruthi Chadran and Brian Gupta who took the time to also run
for DPL this year. Both candidates brought important issues to the forefront
and I hope to work with both of them on those in the near future.

## DPL Blog

Today, I've started a [new blog](https://bits.debian.org/dpl) for the Debian
Project Leader to help facilitate
more frequent communication, and to reach a wider audience via Planet
Debian. This will contain supplemental information to what I send to the
[debian-devel-announce mailing list](https://lists.debian.org/debian-devel-announce/).

## Want to help?

In [my platform](https://www.debian.org/vote/2020/platforms/jcc),
I listed some key areas that I'd like to work on. My work
won't be limited to those, but it should give you some idea of the type of DPL
that I'll be. If you'd like to get involved, feel free to join the #debian-dpl
channel on the oftc IRC network, and please introduce yourself along with any
areas of interest that you'd like to contribute to.

