Title: DPL Activity logs for April/May 2020
Date: 2020-06-03 19:11
Tags: dpl
Slug: dpl-updates-jcc-t01-m01
Author: Jonathan Carter
Status: published

<h3>First month as DPL</h3>

<p>I survived my first month as DPL! I agree with previous DPLs who have described it as starting a whole new job. Fortunately it wasn't very stressful, but it certainly was <em>very</em>
time consuming. On the very first day my inbox exploded with requests. I dealt with this by deferring
anything that wasn't important right away and just started working through it. Fortunately the initial
swell subsided as the month progressed. The bulk of my remaining e-mail backlog are a few media outlets
who wants to do interviews. I'll catch up with those during this month.</p>

<p>Towards the end of the month, most of my focus was on helping to prepare for an <a href="https://wiki.debian.org/DebianEvents/internet/2020/MiniDebConfOnline">online MiniDebConf</a> that we hosted over the last weekend in May. We had lots of fun and we had some great speakers sharing their knowledge and expertise during the weekend.</p>

<h3>Activity log</h3>

<p>As I do on my own blog for free software activities, I'll attempt to keep a log of DPL activities on this blog. Here's the log for the period 2020-04-21 to 2020-05-21:</p>

<p><strong>2020-04-19:</strong> Handover session with Sam, our outgoing DPL. We covered a lot of questions I had and main areas that the DPL works in. Thanks to Sam for having taken the time to do this.</p>

<p><strong>2020-04-21:</strong> First day of term! Thank you to everybody who showed support and have offered to help!</p>

<p><strong>2020-04-21:</strong> Request feedback from the trademark team on an ongoing trademark dispute.</p>

<p><strong>2020-04-21</strong>: Join the GNOME Advisory Board as a representative from Debian.</p>

<p><strong>2020-04-21:</strong> Reply on an ongoing community conflict issue.</p>

<p><strong>2020-04-21:</strong> Update Debian project summary for SPI annual report.</p>

<p><strong>2020-04-21:</strong> Received a great e-mail introduction from Debian France and followed up on that.</p>

<p><strong>2020-04-21:</strong> Posted "<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00013.html">Bits from the new DPL</a>" to debian-devel-announce.</p>

<p><strong>2020-04-22:</strong> Became Debian's OSI Affilliate representative.</p>

<p><strong>2020-04-22:</strong> Reply to a bunch of media inquiries for interviews, will do them later when initial priorities are on track.</p>

<p><strong>2020-04-23:</strong> Resign as Debian FTP team trainee and mailing list moderator. In both these areas there are enough people taking care of it and I intend to maximise available time for DPL and technical areas in the project.</p>

<p><strong>2020-04-25:</strong> Review outgoing mail for trademark team.</p>

<p><strong>2020-04-25:</strong> Answer some questions in preparation for DAM/Front Desk delegation updates.</p>

<p><strong>2020-04-26:</strong> Initiated wiki documentation for <a href="https://wiki.debian.org/Teams/DPL/HowTo/DelegationUpdates">delegation updates process</a>.</p>

<p><strong>2020-04-27:</strong> Update delegation for the<a href="https://lists.debian.org/debian-devel-announce/2020/04/msg00014.html"> Debian Front Desk team</a>.</p>

<p><strong>2020-04-29:</strong> Schedule video call with Debian treasurer team.</p>

<p><strong>2020-04-29:</strong> OSI affiliate call. Learned about some Open Source projects including <a href="https://opendev.org/">OpenDev</a>, <a href="https://www.opensourcematters.org/organisation.html">OpenSourceMatters</a>, <a href="https://fossresponders.com/">FOSS Responders</a> and <a href="https://www.democracylab.org/index/?section=Home">Democracy Labs</a>.</p>

<p><strong>2020-05-04:</strong> Delivered my first talk session as DPL titled <a href="https://peertube.debian.social/videos/watch/cdfe5b24-e3ad-4422-a6f2-15ca4fffd895">"Mixed Bag of Debian"</a> at "Fique Em Casa Use Debian" (Stay at home and use Debian!), organised by <a href="https://peertube.debian.social/accounts/debianbrazilteam/video-channels">Debian Brazil</a>, where they had a different talk every evening during the month of May. Great initiative I hope other local groups consider copying their ideas!</p>

<p><strong>2020-05-05:</strong> Had a 2 hour long call with the treasurer team. Feeling optimistic for the future of Debian's financing although it will take some time and a lot of work to get where we want to be.</p>

<p><strong>2020-05-17:</strong> Respond to cloud delegation update.</p>
