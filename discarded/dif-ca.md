Title: Nova secció: Debian in the Field
Slug: debian-in-the-field
Date: 2015-11-15 09:00
Author: Laura Arjona Reina i Bernelle Verster
Translator: Adrià García-Alzórriz, Lluís Gili
Tags: DiF, Debian in the Field, DebConf16
Lang: ca
Status: draft

«Debian in the Field» (Debian sobre el terreny) mostra algunes aplicacions de Debian 
i més àmpliament, de FLOSS) a un públic majoritari (des de professionals de les TI o 
membres de Debian fins a persones o organitzacions que podrien considerar els ordinadors
com "aquelles coses que fan coses útils".

La web de Debian ja te una secció ["Qui usa 
Debian"](https://www.debian.org/users) ; aquí al blog pretenem oferir
diverses i més detallades experiències d'aquests usuaris.
Compartim aquests usos pràctics per mostrar Debian en ús, i no expressen el punt de
vista o cap recomanació per part del projecte Debian.

Si voleu col·laborar amb articles per aquesta secció «Debian in the FLOSS» visiteu la
[pàgina wiki de bits.debian.org](https://wiki.debian.org/Teams/Publicity/bits.debian.org).
