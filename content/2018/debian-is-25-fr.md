Title: 25 ans et c'est pas fini
Slug: debian-is-25
Date: 2018-08-16 8:50
Author: Ana Guerrero Lopez
Artist: Angelo Rosa
Tags: debian, birthday
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

![Debian a 25 ans par Angelo Rosa](|static|/images/debian25years.png)

Il y a vingt-cinq ans, lorsque le regretté Ian Murdock annonça sur
comp.os.linux.development
*« l'achèvement imminent d'une toute nouvelle version de Linux, [...] la version Debian de Linux »*,
personne n'aurait pu prévoir que la « version Debian de Linux », connue
aujourd'hui sous le nom de Projet Debian, deviendrait l'un des plus
importants et des plus influents projets à code source ouvert. Son principal
produit est Debian, un système d'exploitation (OS) libre pour votre
ordinateur aussi bien que pour tout un tas d'autres systèmes qui améliorent
votre qualité de vie. Depuis le fonctionnement interne de l'aéroport près de
chez vous jusqu'au système multimédia de votre véhicule, et des serveurs de nuage
hébergeant vos sites web favoris jusqu'aux objets connectés qui communiquent
avec eux, Debian peut tous les faire fonctionner.

Aujourd'hui, le projet Debian est une grande organisation florissante avec
d'innombrables équipes auto-organisées constituées de volontaires. Bien
qu'il paraisse souvent chaotique vu de l'extérieur, le projet est soutenu
par ses deux principaux documents structurels : le [Contrat social de Debian]
qui offre une vision pour l'amélioration de la société et les
[Principes du logiciel libre selon Debian] (Debian Free Software Guidelines)
qui fournissent une indication sur les logiciels considérés comme
pouvant être utilisés. Ces documents sont complétés par la [Constitution] du
projet qui formule la structure du projet et le [Code de conduite] qui établit
le ton que doivent avoir les relations à l'intérieur du projet.

Tous les jours durant ces vingt-cinq dernières années, des contributeurs ont
envoyé des rapports de bogue et des correctifs, envoyé des paquets, mis à
jour des traductions, créé des illustrations, organisé des
événements autour de Debian, mis à jour le site web, enseigné à d'autres
comment utiliser Debian et créé des centaines de distributions dérivées.

**Souhaitons-lui vingt-cinq années supplémentaires – et, on l'espère beaucoup, beaucoup plus !**


[Contrat Social de Debian]: https://www.debian.org/social_contract
[Principes du logiciel libre selon Debian]: https://www.debian.org/social_contract#guidelines
[Constitution]: https://www.debian.org/devel/constitution
[Code de conduite]: https://www.debian.org/code_of_conduct
