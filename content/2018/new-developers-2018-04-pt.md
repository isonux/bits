Title: Novos desenvolvedores e mantenedores Debian (março e abril de 2018)
Slug: new-developers-2018-04
Date: 2018-05-02 22:03
Author: Jean-Pierre Giraud
Tags: project
Lang: pt
Translator:
Status: published


Os seguintes colaboradores do projeto se tornaram Desenvolvedores Debian nos últimos dois meses:

  * Andreas Boll (aboll)
  * Dominik George (natureshadow)
  * Julien Puydt (jpuydt)
  * Sergio Durigan Junior (sergiodj)
  * Robie Basak (rbasak)
  * Elena Grandi (valhalla)
  * Peter Pentchev (roam)
  * Samuel Henrique (samueloph)

Os seguintes colaboradores do projeto se tornaram Mantenedores Debian nos últimos dois meses:

  * Andy Li
  * Alexandre Rossi
  * David Mohammed
  * Tim Lunn
  * Rebecca Natalie Palmer
  * Andrea Bolognani
  * Toke Høiland-Jørgensen
  * Gabriel F. T. Gomes
  * Bjorn Anders Dolk
  * Geoffroy Youri Berret
  * Dmitry Eremin-Solenikov

Parabéns a todos!




