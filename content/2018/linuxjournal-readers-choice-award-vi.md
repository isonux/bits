Title: Debian đã đạt danh hiệu Bản phân phối Linux tốt nhất do độc giả tạp chí Linux bình chọn!
Date: 2018-02-14 23:00
Tags: debian, award
Slug: debian-linuxjournal-readers-choice-award
Author: Laura Arjona Reina
Translator: Trần Ngọc Quân
Lang: vi
Status: published

Debian đã đạt danh hiệu [Bản phân phối Linux tốt nhất do độc giả tạp chí Linux bình chọn](http://www.linuxjournal.com/content/best-linux-distribution).

Cảm ơn vì tất cả những đóng góp của bạn!

![Giải thưởng tạp chí Linux](|static|/images/ljaward.png)
