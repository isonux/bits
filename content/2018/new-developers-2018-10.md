Title: New Debian Developers and Maintainers (September and October 2018)
Slug: new-developers-2018-10
Date: 2018-11-08 14:00
Author: Jean-Pierre Giraud
Tags: project
Lang: en
Translator: 
Status: published


The following contributors got their Debian Developer accounts in the last two months:

  * Joseph Herlant (aerostitch)
  * Aurélien Couderc (coucouf)
  * Dylan Aïssi (daissi)
  * Kunal Mehta (legoktm)
  * Ming-ting Yao Wei (mwei)
  * Nicolas Braud-Santoni (nicoo)
  * Pierre-Elliott Bécue (peb)
  * Stephen Gelman (ssgelm)
  * Daniel Echeverry (epsilon)
  * Dmitry Bogatov (kaction)

The following contributors were added as Debian Maintainers in the last two months:

  * Sagar Ippalpalli
  * Kurt Kremitzki
  * Michal Arbet
  * Peter Wienemann
  * Alexis Bienvenüe
  * Gard Spreemann

Congratulations!

