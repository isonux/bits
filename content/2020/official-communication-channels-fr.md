Title: Les canaux de communication officiels de Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announcement
Lang: fr
Translator: Jean-Pierre Giraud
Status: published

De temps à autre, nous sommes interrogés dans Debian sur nos canaux de
communication officiels et sur la position pour Debian de ceux qui peuvent
posséder des sites internet nommés de façon similaire.

Le site web principal de Debian [www.debian.org](https://www.debian.org)
constitue notre moyen de communication fondamental. Tous ceux qui recherchent
des informations sur des événements actuels et le processus de développement
dans la communauté seront intéressés par la section des
[actualités récentes](https://www.debian.org/News) du site de
Debian.

Pour des annonces moins formelles, le projet dispose du blog officiel de Debian
[Bits from Debian](https://bits.debian.org),
et du service [Debian micronews](https://micronews.debian.org)
pour les nouvelles brèves.

La lettre d'information officielle
[Nouvelles du projet Debian](https://www.debian.org/News/weekly/)
et toutes les annonces officielles de nouvelles ou d'évolutions du projet sont
envoyées à la fois sur le site internet et aux listes de diffusion officiel du
projet
[debian-announce](https://lists.debian.org/debian-announce) ou
[debian-news](https://lists.debian.org/debian-news/).
Les envois à ces listes de diffusion sont modérés.

Nous profitons de cette annonce pour indiquer comment le Projet Debian, ou pour
faire court, Debian, est structuré.

La structure de Debian est réglementée par sa
[Constitution](https:/www.debian.org/devel/constitution).
La liste des directeurs et des membres délégués se trouve sur la page
[Structure organisationnelle de Debian](https:/www.debian.org/intro/organization).
D'autres équipes sont listées sur la page [Teams](https://wiki.debian.org/Teams)
du wiki.

Une liste complète des membres officiels de Debian se trouve sur la
[page des nouveaux membres](https://nm.debian.org/members),
où est gérée l'affiliation au projet. Une liste plus large des contributeurs de
Debian se trouve sur la page des [Contributeurs](https://contributors.debian.org).

Si vous avez des questions, nous vous invitons à vous adresser à l'équipe
« presse » à l'adresse [press@debian.org](mailto:press@debian.org).

