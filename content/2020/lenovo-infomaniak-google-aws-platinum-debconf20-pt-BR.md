Title: Lenovo, Infomaniak, Google e Amazon Web Services (AWS), patrocinadores(as) Platinum da DebConf20
Slug: lenovo-infomaniak-google-aws-platinum-debconf20
Date: 2020-08-20 20:25
Author: Laura Arjona Reina
Artist: Lenovo, Infomaniak, Google, AWS
Tags: debconf20, debconf, sponsors, lenovo, infomaniak, google, aws
Lang: pt-BR
Translator: Thiago Pezzo (Tico)
Status: published


Estamos muito felizes em anunciar que [**Lenovo**](https://www.lenovo.com),
[**Infomaniak**](https://www.infomaniak.com),
[**Google**](https://google.com)
e [**Amazon Web Services (AWS)**](https://aws.amazon.com)
se comprometeram a apoiar a [DebConf20](https://debconf20.debconf.org) como **patrocinadores Platinum**.


[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com)

Como uma líder global em tecnologia, fabricando um amplo portfólio de produtos
conectados, incluindo smartphones, tablets, PCs e workstations, bem como
dispositivos de realidade aumentada/virtual, casas/escritórios inteligentes e
soluções para datacenters, a [**Lenovo**](https://www.lenovo.com) entende quão
críticos são os sistemas abertos e as plataformas para um mundo conectado.


[![infomaniaklogo](|static|/images/infomaniak.png)](https://www.infomaniak.com)

[**Infomaniak**](https://www.infomaniak.com) é a maior empresa de hospedagem
web da Suíça, que também oferece serviços de backup e de armazenamento, soluções
para organizadores(as) de eventos, serviços de transmissão em tempo real e vídeo
sob demanda.
Todos os datacenters e elementos críticos para o funcionamento dos serviços e
produtos oferecidos pela empresa (tanto software quanto hardware) são de
propriedade absoluta da Infomaniak.


[![Googlelogo](|static|/images/google.png)](https://www.google.com)

[**Google**](https://google.com/) é uma das maiores empresas de tecnologia do
mundo, fornecendo uma ampla gama de serviços e produtos relacionados à Internet
como tecnologias de publicidade on-line, pesquisa, computação em nuvem,
software e hardware.

O Google tem apoiado o Debian através do patrocínio da DebConf por mais de
dez anos, e é também um parceiro Debian que patrocina partes da
infraestrutura de integração contínua do [Salsa](https://salsa.debian.org)
dentro da Plataforma Google Cloud.


[![AWSlogo](|static|/images/aws.png)](https://aws.amazon.com)

[**Amazon Web Services (AWS)**](https://aws.amazon.com) é uma das plataformas
em nuvem mais abrangentes e largamente adotadas no mundo,
oferecendo mais de 175 serviços repletos de funcionalidades via datacenters
globais (em 77 zonas de disponibilidade dentro de 24 regiões geográficas).
Consumidores(as) da AWS incluem as startups de mais rápido crescimento, as
maiores empresas e as agências governamentais de liderança.


Com esses comprometimentos enquanto patrocinadores(as) Platinum,
Lenovo, Infomaniak, Google e Amazon Web Services estão contribuindo
para tornar possível nossa conferência anual,
e diretamente apoiando o progresso do Debian e do Software Livre,
ajudando a fortalecer a comunidade que continua a colaborar nos
projetos do Debian durante todo o resto do ano.

Muito obrigado(a) pelo seu apoio à DebConf20!

## Participando da DebConf20 on-line

A 21a Conferência Debian acontecerá on-line, devido ao COVID-19,
de 23 a 29 de agosto de 2020. Serão 7 dias de atividades, acontecendo das
10h à 1h UTC (7h às 21h no horário de Brasília).
Visite o site web da DebConf20 em [https://debconf20.debconf.org](https://debconf20.debconf.org)
para conhecer a programação completa, assistir as transmissões ao vivo e
juntar-se aos diferentes canais de comunicação para participar da conferência.
