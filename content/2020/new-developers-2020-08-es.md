Title: Nuevos mantenedores de Debian (julio y agosto del 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: es
Translator: 
Status: draft


Los siguientes colaboradores del proyecto se convirtieron en Debian Maintainers en los dos últimos meses:

  * Chirayu Desai
  * Shayan Doust
  * Arnaud Ferraris
  * Fritz Reichwald
  * Kartik Kulkarni
  * François Mazen
  * Patrick Franz
  * Francisco Vilmar Cardosa Ruviaro
  * Octavio Alvarez
  * Nick Black

¡Felicidades a todos!

