Title: Nouveaux mainteneurs de Debian (juillet et août 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: draft


Les contributeurs suivants ont été acceptés comme mainteneurs Debian ces deux derniers mois :

  * Chirayu Desai
  * Shayan Doust
  * Arnaud Ferraris
  * Fritz Reichwald
  * Kartik Kulkarni
  * François Mazen
  * Patrick Franz
  * Francisco Vilmar Cardosa Ruviaro
  * Octavio Alvarez
  * Nick Black

Félicitations !

