Title: Report of the Debian Perl Sprint 2020
Slug: debian-perl-sprint-2020
Date: 2020-06-15 13:40
Author: Dominic Hargreaves
Tags: perl, sprint
Status: published


Eight members of the [Debian Perl](https://wiki.debian.org/Teams/DebianPerlGroup) team met online between May 15 and May 17 2020, in lieu of a planned physical sprint meeting. Work focussed on preparations for bullseye,
and continued maintenance of the large number of perl modules maintained
by the team.

Whilst an online sprint cannot fully replace an in-person sprint
in terms of focussing attention, the weekend was still very
productive, and progress was made on a range of topics including:

* Reducing technical debt by removing unmaintained packages
* Beginning packaging and QA for the next major release of perl, 5.32
* Deciding on a team policy for hardening flags
* Addressing concerns with `Alien::*`, a set of pacakges designed to download source code
* Developing a proposal for debian/NEWS.Developer, to complement
  debian/NEWS
* Developing a plan to enable SSL verification in HTTP::Tiny
  by default

The [full report](https://lists.debian.org/debian-perl/2020/05/msg00051.html) was posted to the relevant Debian mailing lists.

The participants would like to thank [OpusVL](https://opusvl.com/)
for providing the Jitsi instance for the weekend.
