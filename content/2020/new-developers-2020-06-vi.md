Title: Những nhà Phát triển và Bảo trì Debian mới (Tháng năm và sáu 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: vi
Translator: Trần Ngọc Quân
Status: published


Trong hai tháng qua, những người đóng góp sau đây nhận được tài khoản Phát triển Debian:

  * Richard Laager (rlaager)
  * Thiago Andrade Marques (andrade)
  * Vincent Prat (vivi)
  * Michael Robin Crusoe (crusoe)
  * Jordan Justen (jljusten)
  * Anuradha Weeraman (anuradha)
  * Bernelle Verster (indiebio)
  * Gabriel F. T. Gomes (gabriel)
  * Kurt Kremitzki (kkremitzki)
  * Nicolas Mora (babelouest)
  * Birger Schacht (birger)
  * Sudip Mukherjee (sudip)

Trong hai tháng qua, những người đóng góp sau đây được thêm vào với tư cách là người Bảo trì Debian:

  * Marco Trevisan
  * Dennis Braun
  * Stephane Neveu
  * Seunghun Han
  * Alexander Johan Georg Kjäll
  * Friedrich Beckmann
  * Diego M. Rodriguez
  * Nilesh Patra
  * Hiroshi Yokota

Xin chúc mừng!

