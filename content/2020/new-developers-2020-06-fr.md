Title: Nouveaux développeurs et mainteneurs de Debian (mai et juin 2020)
Slug: new-developers-2020-06
Date: 2020-07-21 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: Jean-Pierre Giraud
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Richard Laager (rlaager)
  * Thiago Andrade Marques (andrade)
  * Vincent Prat (vivi)
  * Michael Robin Crusoe (crusoe)
  * Jordan Justen (jljusten)
  * Anuradha Weeraman (anuradha)
  * Bernelle Verster (indiebio)
  * Gabriel F. T. Gomes (gabriel)
  * Kurt Kremitzki (kkremitzki)
  * Nicolas Mora (babelouest)
  * Birger Schacht (birger)
  * Sudip Mukherjee (sudip)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Marco Trevisan
  * Dennis Braun
  * Stephane Neveu
  * Seunghun Han
  * Alexander Johan Georg Kjäll
  * Friedrich Beckmann
  * Diego M. Rodriguez
  * Nilesh Patra
  * Hiroshi Yokota

Félicitations !

