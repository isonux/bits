Title: Canales oficiales de comunicación de DebianOfficial communication channels for Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announcement
Lang: es
Translator: Laura Arjona Reina
Status: published

De cuando en cuando, recibimos preguntas en Debian acerca de nuestros canales
oficiales de comunicación y consultas acerca de la relación con Debian de
quien gestiona sitios web con nombres parecidos.

El principal sitio web de Debian [www.debian.org](https://www.debian.org)
es nuestro medio de comunicación primordial. 
Aquellos que busquen información sobre lo que sucede actualmente y lo que está
en desarrollo en la comunidad de Debian puede que estén interesados en la sección
de [noticias del proyecto Debian](https://www.debian.org/News/) en la web.

Para anuncios menos formales, tenemos el blog oficial de Debian
[Bits from Debian](https://bits.debian.org),
y el servicio de [micronoticias de Debian, «Debian micronews»](https://micronews.debian.org)
para noticias más cortas (publicadas en inglés).

Nuestro boletín oficial 
[Noticias del proyecto Debian](https://www.debian.org/News/weekly/)
y todos los anuncios oficiales de noticias o cambios en el proyecto se publican
en nuestro sitio web y se envían a nuestras listas oficiales
[debian-announce](https://lists.debian.org/debian-announce/) o
[debian-news](https://lists.debian.org/debian-news/).
El envío a esas listas de correo está restringido.

También queremos aprovechar la oportunidad para anunciar cómo el proyecto Debian,
o para acortar, Debian, está estructurado.


Debian tiene una estructura regulada por nuestra
[constitución](https://www.debian.org/devel/constitution).
Los directores y miembros delegados están listados en la página sobre
[estructura organizativa](https://www.debian.org/intro/organization).
Los equipos adicionales están listados en nuestra página wiki de
[Equipos](https://wiki.debian.org/Teams).


La lista completa de miembros oficiales de Debian puede encontrarse en nuestra
[página de nuevos miembros](https://nm.debian.org/members),
donde se gestiona la membresía. Una lista más amplia de contribuidores a Debian
puede encontrarse en nuestra página de
[contribuidores](https://contributors.debian.org).

Si tiene preguntas, le invitamos a contactar con el equipo de prensa en
[press@debian.org](mailto:press@debian.org).
