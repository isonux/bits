Title: Debian Local Groups at DebConf20 and beyond
Slug: debian-local-groups-debconf20
Date: 2020-09-16 19:15
Author: Francisco M. Neto and Laura Arjona Reina
Tags: 
Status: published

There are a number of large and very successful Debian Local Groups
(Debian France, Debian Brazil and Debian Taiwan, just to name a few),
but what can we do to help support upcoming local groups or help spark interest
in more parts of the world?

There has been a session about
[Debian Local Teams at Debconf20](https://debconf20.debconf.org/talks/50-local-teams/)
and it generated generated quite a bit of constructive discussion in the live stream
(recording available at 
[https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/](https://meetings-archive.debian.net/pub/debian-meetings/2020/DebConf20/)),
in [the session's Etherpad](https://pad.online.debconf.org/p/50-local-teams)
and in the IRC channel (#debian-localgroups). This article is an attempt at
summarizing the key points that were raised during that discussion, as well as
the plans for the future actions to support new or existent Debian Local Groups
and the possibility of setting up a local group support team.

## Pandemic situation

During a pandemic it may seem strange to discuss offline meetings, but this is
a good time to be planning things for the future. At the same time, the current
situation makes it more important than before to encourage local interaction.

## Reasoning for local groups

Debian can seem scary for those outside. Already having a connection to Debian - especially
to people directly involved in it - seems to be the way through
which most contributors arrive. But if one doesn't have a connection,
it is not that easy; Local Groups facilitate that by improving networking.

Local groups are incredibly important to the success of Debian
since they often help with translations, making us more diverse, support,
setting up local bug squashing sprints, establishing a local DebConf team
along with miniDebConfs, getting sponsors for the project and much more. 

Existence of a Local Groups would also facilitate access to "swag" like stickers
and mugs, since people not always have the time to deal with the process
of finding a supplier to actually get those made.
The activity of local groups might facilitate that by organizing related logistics. 

## How to deal with local groups, how to define a local group

Debian gathers the information about Local Groups in its
[Local Groups wiki page](https://wiki.debian.org/LocalGroups) (and subpages).
Other organisations also have their own schemes, some of them featuring a map,
blogs, or clear rules about what constitutes a local group. In the case of Debian
there is not a predefined set of "rules", even about the group name.
That is perfectly fine, we assume that certain local groups may be very small,
or temporary (created around a certain time when they plan several activities,
and then become silent). However, the way the groups are named and how they are
listed on the wiki page sets expectations with regards to what kinds of activities
they involve. 

For this reason, we encourage all the Debian Local Groups to review their entries
in the [Debian wiki](https://wiki.debian.org/LocalGroups), keep it current
(e.g. add a line "Status: Active (2020)), and we encourage informal groups of
Debian contributors that somehow "meet", to create a new entry in the wiki page, too.

## What can Debian do to support Local Groups

Having a centralized database of groups is good (if up-to-date),
but not enough. We'll explore other ways of propagation and increasing visibility,
like organising the logistics of printing/sending swag and facilitate access
to funding for Debian-related events.

## Continuation of efforts

Efforts shall continue regarding Local Groups.
Regular meetings are happening every two or three weeks;
interested people are encouraged to explore some other relevant DebConf20 talks
([Introducing Debian Brasil](https://debconf20.debconf.org/talks/105-introducing-debian-brasil/),
[Debian Academy: Another way to share knowledge about Debian](https://debconf20.debconf.org/talks/30-debian-academy-another-way-to-share-knowledge-about-debian/),
[An Experience creating a local community on a small town](https://debconf20.debconf.org/talks/55-an-experience-creating-a-local-community-in-a-small-town/)),
websites like [Debian flyers](https://debian.pages.debian.net/debian-flyers/)
(including other printed material as cube, stickers),
visit the [events section of the Debian website](https://www.debian.org/events/)
and the [Debian Locations](https://wiki.debian.org/DebianLocations) wiki page,
and participate in the IRC channel #debian-localgroups at OFTC.
