Title: Nous mantenidors de Debian (juliol i agost del 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Translator: 
Status: draft


Els següents col·laboradors del projecte han esdevingut Debian Maintainers en els darrers dos mesos:

  * Chirayu Desai
  * Shayan Doust
  * Arnaud Ferraris
  * Fritz Reichwald
  * Kartik Kulkarni
  * François Mazen
  * Patrick Franz
  * Francisco Vilmar Cardosa Ruviaro
  * Octavio Alvarez
  * Nick Black

Enhorabona a tots!

