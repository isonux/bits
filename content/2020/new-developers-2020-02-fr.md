Title: Nouveaux développeurs et mainteneurs de Debian (janvier et février 2020)
Slug: new-developers-2020-02
Date: 2020-03-23 15:00
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Translator: 
Status: published


Les contributeurs suivants sont devenus développeurs Debian ces deux derniers mois :

  * Gard Spreemann (gspr)
  * Jonathan Bustillos (jathan)
  * Scott Talbert (swt2c)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la même période :

  * Thiago Andrade Marques
  * William Grzybowski
  * Sudip Mukherjee
  * Birger Schacht
  * Michael Robin Crusoe
  * Lars Tangvald
  * Alberto Molina Coballes
  * Emmanuel Arias
  * Hsieh-Tseng Shen
  * Jamie Strandboge

Félicitations !

