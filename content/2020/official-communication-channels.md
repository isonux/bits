Title: Official communication channels for Debian
Slug: official-communication-channels
Date: 2020-03-16 14:00
Author: Laura Arjona Reina, Ana Guerrero Lopez and Donald Norwood
Tags: project, announcement
Status: published

From time to time, we get questions in Debian about our official channels of
communication and questions about the Debian status of who may own similarly
named websites.

The main Debian website [www.debian.org](https://www.debian.org)
is our primary medium of communication. Those seeking information about current
events and development progress in the community may be interested in the
[Debian News](https://www.debian.org/News/) section of the Debian
website.
For less formal announcements, we have the official Debian blog
[Bits from Debian](https://bits.debian.org), and the
[Debian micronews](https://micronews.debian.org)
service for shorter news items.

Our official newsletter
[Debian Project News](https://www.debian.org/News/weekly/)
and all official announcements of news or project changes are dual posted on
our website and sent to our official mailing lists
[debian-announce](https://lists.debian.org/debian-announce/) or
[debian-news](https://lists.debian.org/debian-news/).
Posting to those mailing lists is restricted.

We also want to take the opportunity to announce how the Debian Project,
or for short, Debian is structured.

Debian has a structure regulated by our
[Constitution](https://www.debian.org/devel/constitution).
Officers and delegated members are listed on our
[Organizational Structure](https://www.debian.org/intro/organization) page.
Additional teams are listed on our [Teams](https://wiki.debian.org/Teams) page.

The complete list of official Debian members can be found on our
[New Members page](https://nm.debian.org/members),
where our membership is managed. A broader list of Debian contributors can be
found on our [Contributors](https://contributors.debian.org) page.

If you have questions, we invite you to reach the press team at
[press@debian.org](mailto:press@debian.org).
