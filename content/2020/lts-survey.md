Title: Debian Long Term Support (LTS) users and contributors survey
Slug: lts-survey
Date: 2020-07-13 14:00
Author: Holger Levsen
Tags: project, announcement
Status: published

On July 18th Stretch LTS starts, offering two more years of security support
to the Debian Stretch release. Stretch LTS will be the fourth iteration of LTS,
following Squeeze LTS which started in 2014, Wheezy LTS in 2016 and Jessie LTS
in 2018.

However, for the first time, we have prepared a small survey about our
users and contributors, who they are and why they are using LTS.

Filling out the survey should take less than 10 minutes. We would really
appreciate if you could [participate in the survey online](https://surveys.debian.net/index.php?r=survey/index&sid=856794&lang=en)!
    
In two weeks (July 27th 2020) we will close the survey, so please don't
hesitate and participate now! After that, there will be a followup email with
the results.

More information about Debian LTS is available at [https://wiki.debian.org/LTS](https://wiki.debian.org/LTS),
including generic contact information.

[Click here to fill out the survey now](https://surveys.debian.net/index.php?r=survey/index&sid=856794&lang=en)!
