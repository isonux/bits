Title: Nya Debian Maintainers (Juli och Augusti 2020)
Slug: new-developers-2020-08
Date: 2020-09-19 16:00
Author: Jean-Pierre Giraud
Tags: project
Lang: sv
Translator: Andreas Rönnquist
Status: draft


Följande bidragslämnare adderades som Debian Maintainers under de senaste två månaderna:

  * Chirayu Desai
  * Shayan Doust
  * Arnaud Ferraris
  * Fritz Reichwald
  * Kartik Kulkarni
  * François Mazen
  * Patrick Franz
  * Francisco Vilmar Cardosa Ruviaro
  * Octavio Alvarez
  * Nick Black

Grattis!

