Title: DebConf19 começa hoje em Curitiba
Slug: debconf19-starts
Date: 2019-07-21 21:10
Author: Laura Arjona Reina
Tags: debconf19, debconf
Lang: pt-BR
Translator: Carlos Filho
Status: published

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

[DebConf19](https://debconf19.debconf.org), 20º Conferência Anual do Debian,
está sendo realizada em Curitiba, Brasil de 21 a 28 de julho de 2019.

Contribuidores Debian de todo o mundo se reuniram na 
[Universidade Federal de Tecnologia - Paraná (UTFPR)](https://debconf19.debconf.org/about/venue/)
em Curitiba, Brasil, para participar e trabalhar em uma conferência
exclusivamente executada por voluntários.

Hoje a conferência principal começa, são esperados mais de 350 participantes
e estão 121 atividades programadas, incluindo palestras de 45 e 20 minutos
e reuniões de equipe ("BoF"), oficinas, uma feira de empregos,
bem como uma variedade de outros eventos.

A programação completa em esta no endereço
[https://debconf19.debconf.org/schedule/](https://debconf19.debconf.org/schedule/)
é atualizada todos os dias, incluindo atividades planejadas e formadas
pelos participantes durante toda a conferência.

Se você quiser se envolver remotamente, você pode seguir o 
[**streaming de vídeo** disponível no site da DebConf19](https://debconf19.debconf.org/)
dos eventos que acontecem nas três salas de conversação:
_Auditório_ (auditório principal), _Miniauditório_ e _Sala de Videoconferência_.
ou participar das conversas sobre o que está acontecendo nas salas de conversação: 
  [**#debconf-auditorio**](https://webchat.oftc.net/?channels=#debconf-auditorio),
  [**#debconf-miniauditorio**](https://webchat.oftc.net/?channels=#debconf-miniauditorio) e
  [**#debconf-videoconferencia**](https://webchat.oftc.net/?channels=#debconf-videoconferencia)
(todos esses canais na rede OFTC IRC).

Você também pode acompanhar a cobertura ao vivo de notícias sobre DebConf19 em
[https://micronews.debian.org](https://micronews.debian.org) ou o @debian perfil na sua rede social favorita.

A DebConf está comprometida com um ambiente seguro e bem-vindo para todos os participantes.
Durante a conferência, várias equipes (Front Desk, equipe de boas-vindas e equipe anti-assédio)
estão disponíveis para ajudar, para que os participantes no local e remotos tenham
a melhor experiência possível na conferência e encontrar soluções para qualquer problema que possa surgir.
Veja a [página web sobre o Código de Conduta no site da DebConf19](https://debconf19.debconf.org/about/coc/)
para mais detalhes sobre isso.


O Debian agradece aos inúmeros [patrocinadores](https://debconf19.debconf.org/sponsors/)
por seu compromisso com DebConf19, particularmente seus patrocinadores Platinum:
[Infomaniak](https://www.infomaniak.com),
[Google](https://google.com/)
e [Lenovo](https://www.lenovo.com).
