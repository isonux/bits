Title: "futurePrototype" will be the default theme for  Debian 10
Slug: futurePrototype-will-be-the-default-theme-for-debian-10
Date: 2019-01-14 13:15
Author: Laura Arjona Reina, Niels Thykier and Jonathan Carter
Artist: Alex Makas
Tags: buster, artwork
Status: published

The theme ["futurePrototype"][futureprototype-link]
by Alex Makas has been selected
as default theme for Debian 10 'buster'.

[![futurePrototype Login screen. Click to see the whole theme proposal](|static|/images/futureprototype_login.png)][futureprototype-link]

[futureprototype-link]: https://wiki.debian.org/DebianArt/Themes/futurePrototype

After the Debian Desktop Team made the
[call for proposing themes](https://lists.debian.org/debian-devel-announce/2018/06/msg00003.html),
a total of [eleven choices](https://wiki.debian.org/DebianDesktop/Artwork/buster)
have been submitted, and any Debian contributor has received
the opportunity to vote on them in a survey.
We received 3,646 responses ranking the different choices,
and futurePrototype has been the winner among them.

We'd like to thank all the designers that have participated
providing nice wallpapers and artwork for Debian 10,
and encourage everybody interested in this area of Debian,
to join the [Design Team](https://wiki.debian.org/Design).

Congratulations, Alex, and thank you very much for your contribution to Debian!
