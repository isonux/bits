Title: Debian welcomes its new Outreachy interns
Slug: welcome-outreachy-interns-2019-2020
Date: 2019-11-29 11:20
Author: Daniel Lange, Laura Arjona Reina
Tags: announce, outreachy
Artist: Outreachy
Status: published

![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Debian continues participating in Outreachy, and we'd like to
welcome our new Outreachy interns for this round, lasting from December 2019 to
March 2020.

[Anisa Kuci](https://anisakuci.com/)
will work on
[Improving the DebConf fundraising processes](https://www.outreachy.org/december-2019-to-march-2020-internship-round/communities/debian/#create-fundraising-material-for-debconf20-document),
mentored by Karina Ture and Daniel Lange.

[Sakshi Sangwan](https://github.com/sakshisangwan)
will work on
[Packaging GitLab's JS Modules](https://www.outreachy.org/december-2019-to-march-2020-internship-round/communities/debian/#packaging-gitlabs-js-modules),
mentored by Utkarsh Gupta, Sruthi Chandran and Pirate Praveen.

Congratulations, Anisa and Sakshi! Welcome!

From [the official website](https://www.outreachy.org/): *Outreachy provides three-month internships
for people from groups traditionally underrepresented in tech.
Interns work remotely with mentors from Free and Open Source Software (FOSS) communities
on projects ranging from programming, user experience, documentation,
illustration and graphical design, to data science.*

The Outreachy programme is possible in Debian thanks
to the efforts of Debian developers and contributors who dedicate
their free time to mentor students and outreach tasks, and
the [Software Freedom Conservancy](https://sfconservancy.org/)'s administrative support,
as well as the continued support of Debian's donors, who provide funding
for the internships.

Join us and help extend Debian! You can follow the work of the Outreachy
interns reading their blogs (they are syndicated in [Planet Debian][planet]),
and chat with us in the #debian-outreach IRC channel and [mailing list](https://lists.debian.org/debian-outreach/).

[planet]: https://planet.debian.org
