Title: Debian 10 "buster" has been released!
Date: 2019-07-07 03:25
Tags: buster
Slug: buster-released
Author: Ana Guerrero Lopez, Laura Arjona Reina and Jean-Pierre Giraud
Artist: Alex Makas
Status: published

[![Alt Buster has been released](|static|/images/upcoming-buster.png)](https://deb.li/buster)

You've always dreamt of a faithful pet? He is here, and his name is Buster!
We're happy to announce the release of Debian 10, codenamed *buster*.

**Want to install it?**
Choose your favourite [installation media](https://www.debian.org/distrib/)
and read the [installation manual](https://www.debian.org/releases/buster/installmanual).
You can also use an official cloud image directly on your cloud provider,
or try Debian prior to installing it using our "live" images.

**Already a happy Debian user and you only want to upgrade?**
You can easily upgrade from your current Debian 9 "stretch" installation;
please read the [release notes](https://www.debian.org/releases/buster/releasenotes).

**Do you want to celebrate the release?**
We provide some [buster artwork](https://wiki.debian.org/DebianArt/Themes/futurePrototype) that you
can share or use as base for your own creations. Follow the conversation about buster in social media via the #ReleasingDebianBuster
and #Debian10Buster hashtags or join an in-person or online [Release Party](https://wiki.debian.org/ReleasePartyBuster)!
