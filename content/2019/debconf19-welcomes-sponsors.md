Title: DebConf19 welcomes its sponsors!
Slug: debconf19-welcomes-sponsors
Date: 2019-06-11 14:20
Author: znoteer and Laura Arjona Reina
Tags: debconf19, debconf, sponsors
Status: published

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)

DebConf19 is taking place in Curitiba, Brazil, from 21 July to 28 July 2019.
It is the 20th edition of the Debian conference and organisers are working hard
to create another interesting and fruitful event for attendees.

We would like to warmly welcome the first 29 sponsors of DebConf19, and introduce you to them.

So far we have three Platinum sponsors.

Our first Platinum sponsor is  [**Infomaniak**](https://www.infomaniak.com/en).
Infomaniak is Switzerland's largest web-hosting company,
also offering backup and storage services, solutions for event organizers,
live-streaming and video on demand services.
It wholly owns its datacenters and all elements critical 
to the functioning of the services and products provided by the company 
(both software and hardware).

Next, as a Platinum sponsor, is [**Google**](https://google.com).
Google is one of the largest technology companies in the
world, providing a wide range of Internet-related services and products
as online advertising technologies, search, cloud computing, software, and hardware.
Google has been supporting Debian by sponsoring DebConf since more than
ten years, and is also a [Debian partner](https://www.debian.org/partners/).

[**Lenovo**](https://www.lenovo.com) is our third Planinum sponsor.
Lenovo is a global technology leader manufacturing a wide portfolio of connected
products, including smartphones, tablets, PCs and workstations as well as AR/VR 
devices, smart home/office solutions and data center solutions. This is their first
year sponsoring DebConf.

Our Gold sponsor is [**Collabora**](https://www.collabora.com/),
a global consultancy delivering Open Source software solutions to the commercial world.
Their expertise spans all key areas of Open Source software development.
In addition to offering solutions to clients, Collabora's engineers and developers actively contribute to many Open Source projets.

Our Silver sponsors are:
[**credativ**](http://www.credativ.de/)
(a service-oriented company focusing on open-source software and also a
[Debian development partner](https://www.debian.org/partners/)),
[**Cumulus Networks**](https://cumulusnetworks.com/),
(a company building web-scale networks using innovative, open networking technology),
[**Codethink**](https://www.codethink.co.uk/)
(specialists in system-level software infrastructure supporting advanced technical applications),
the [**Bern University of Applied Sciences**](https://www.bfh.ch/)
(with over [6,800](https://www.bfh.ch/en/bfh/facts_figures.html) students enrolled,
located in the Swiss capital),
[**Civil Infrastructure Platform**](https://www.cip-project.org/),
(a collaborative project hosted by the Linux Foundation,
establishing an open source “base layer” of industrial grade software),
[**\WIT**](https://www.wit.com/)
(offering a secure cloud solution and complete data privacy via Kubnernetes encrypted hardware virtualisation),
[**Hudson-Trading**](http://www.hudson-trading.com/),
(a company researching and developing automated trading algorithms
using advanced mathematical techniques),
[**Ubuntu**](https://www.canonical.com/),
(the Operating System delivered by Canonical),
[**NHS**](https://nhs.com.br/)
(with a broad product portfolio, they offer solutions, amongst others,
for data centres, telecommunications, CCTV, and residential, commercial and industrial automation),
[**rentcars.com**](https://www.rentcars.com/)
who helps customers find the best car rentals from over 100 rental companies
at destinations in the Americas and around the world,
and [**Roche**](https://code4life.roche.com/),
a major international pharmaceutical provider and research company
dedicated to personalized healthcare.

Bronze sponsors:
[**4Linux**](https://www.4linux.com.br),
[**IBM**](https://www.ibm.com/),
[**zpe**](https://www.zpesystems.com),
[**Univention**](https://www.univention.com/),
[**Policorp**](https://www.policorp.com.br),
[**Freexian**](https://www.freexian.com/services/debian-lts.html),
[**globo.com**](https://www.globo.com/).


And finally, our Supporter level sponsors:
[**Altus Metrum**](https://altusmetrum.org/),
[**Pengwin**](https://www.pengwin.dev/),
[**ISG.EE**](https://isg.ee.ethz.ch/),
[**Jupter**](https://jupter.co/),
[**novatec**](https://novatec.com.br/),
[**Intnet**](https://intnet.com.br/),
[**Linux Professional Institute**](https://www.lpi.org/).

Thanks to all our sponsors for their support!
Their contributions make it possible for a large number
of Debian contributors from all over the globe to work together,
help and learn from each other in DebConf19.

## Become a sponsor too!

DebConf19 is still accepting sponsors.
Interested companies and organizations may contact the DebConf team
through [sponsors@debconf.org](mailto:sponsors@debconf.org), and
visit the DebConf19 website at [https://debconf19.debconf.org](https://debconf19.debconf.org).
