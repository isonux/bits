Title: El proyecto Debian apoya a la fundación GNOME en su defensa contra los trols de patentes
Slug: gnome-foundation-defense-patent-troll
Date: 2019-10-23 10:00
Author: Ana Guerrero López
Tags: debian, gnome, patent trolls, fundraising
Translator: Laura Arjona Reina
Lang: es
Status: published

En 2012, el proyecto Debian publicó nuestra [posición acerca de las patentes de software],
exponiendo la amenaza que suponen las patentes para el software libre.

La fundación GNOME ha anunciado recientemente que están afrontando un litigio
en el cual se alega que Shotwell, un gestor de fotos personal de software libre,
infringe una patente.

El proyecto Debian apoya firmemente a la fundación GNOME en sus esfuerzos por mostrar
al mundo que las comunidades de software libre nos defenderemos vigorosamente
ante cualquier abuso del sistema de patentes.

Por favor, lea [este artículo de blog sobre la defensa de GNOME contra este trol de patentes]
y consideren realizar una donación al [fondo de GNOME de defensa contra troles de patentes].

[posición acerca de las patentes de software]: https://www.debian.org/legal/patent
[este artículo de blog sobre la defensa de GNOME contra este trol de patentes]:  https://www.gnome.org/news/2019/10/gnome-files-defense-against-patent-troll/
[fondo de GNOME de defensa contra troles de patentes]: https://secure.givelively.org/donate/gnome-foundation-inc/gnome-patent-troll-defense-fund
