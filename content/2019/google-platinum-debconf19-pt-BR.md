Title: Google Patrocinador Platinum da DebConf19
Slug: google-platinum-debconf19
Date: 2019-03-25 12:30
Author: Laura Arjona Reina
Artist: Google
Tags: debconf19, debconf, sponsors, Google
Lang: pt-BR
Translator: Daniel Pimentel
Status: published

[![Googlelogo](|static|/images/google.png)](https://www.google.com)

Estamos muito felizes em anunciar que
o [**Google**](https://www.google.com) comprometeu-se a apoiar
a [DebConf19](https://debconf19.debconf.org) como um **patrocinador
Platinum**.

*"A DebConf é uma parte importante do ecosistema do desenvolvimento do Debian
e o Google tem o prazer de retornar como um patrocinador em apoio ao trabalho
da comunidade global de voluntários que fazem do Debian e da DebConf uma
realidade"* disse Cat Allman, Gerente do Programa de Código Aberto e equipe de
Criação & Ciência do Google.

O Google é uma das maiores empresas de tecnologia do mundo, fornecendo uma
ampla gama de serviços e produtos relacionados à internet como tecnologias de
publicidade on-line, pesquisa, computação em nuvem, software e hardware.

O Google tem apoiado o Debian através do patrocínio da DebConf a mais de dez
anos, e é também um parceiro Debian que patrocina parte da infraestrutura de
integração contínua do [Salsa](https://salsa.debian.org) na plataforma em nuvem
da Google (Google Cloud).

Com este compromisso adicional como Patrocinador Platinum da DebConf19, o
Google contribue para tornar possível a nossa Conferência anual, e apoia
diretamente o progresso do Debian e do Software Livre ajudando a fortalecer
a comunidade que continua a colaborar nos projetos Debian durante o resto do ano.

Muito obrigado ao Google por apoiar a DebConf19!

## Torne-se um patrocinador também!

A DebConf19 ainda está aceitando patrocinadores. Empresas e
organizações interessadas devem entrar em contato com o time da
DebConf através
do [sponsors@debconf.org](mailto:sponsors@debconf.org), e visitar o
website da DebConf19
em [https://debconf19.debconf.org](https://debconf19.debconf.org).
