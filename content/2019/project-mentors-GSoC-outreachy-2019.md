Title: Projects and mentors for Debian's Google Summer of Code 2019 and Outreachy
Slug: project-and-mentors-gsoc-2019-outreachy
Date: 2019-02-03 10:40
Author: Alexander Wirt
Artist: Google, Outreachy
Tags: gsoc, outreachy
Status: published

![GSoC logo](|static|/images/gsoc.jpg)
![Outreachy logo](|static|/images/outreachy-logo-300x61.png)

Debian is applying as a mentoring organization for the
[Google Summer of Code 2019](https://wiki.debian.org/SummerOfCode2019),
an internship program open to university students aged 18 and up,
and will apply soon for the next round of [Outreachy](https://www.outreachy.org), 
an internship program for people from groups traditionally underrepresented in tech.

Please join us and help expanding Debian and mentoring new free software contributors!

If you have a 
[project idea related to Debian](https://wiki.debian.org/SummerOfCode2019#Guidelines_for_GSoC_Projects_in_Debian) 
and can mentor (or can coordinate the mentorship with some other Debian 
Developer or contributor, or within a Debian team), 
please add the details to the 
[Debian GSoC2019 Projects wiki page](https://wiki.debian.org/SummerOfCode2019/Projects) 
*by Tuesday, February 5 2019*.

Participating in these programs has many benefits for Debian and the wider free
software community.  If you have questions, please come and ask us on
IRC #debian-outreach or the [debian-outreach mailing list]( https://lists.debian.org/debian-outreach/).
