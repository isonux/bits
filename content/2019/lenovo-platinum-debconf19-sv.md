Title: Lenovo platinasponsor av DebConf19
Slug: lenovo-platinum-debconf19
Date: 2019-05-20 10:00
Author: Laura Arjona Reina
Artist: Lenovo
Tags: debconf19, debconf, sponsors, lenovo
Lang: sv
Translator: Andreas Rönnquist
Status: published

[![lenovologo](|static|/images/lenovo.png)](https://www.lenovo.com/)

Vi är mycket glada att kunna tillkännage att [**Lenovo**](https://www.lenovo.com/)
har åtagit sig att stödja [DebConf19](https://debconf19.debconf.org) som en **Platinasponsor**.

*"Lenovo sponsrar stolt den 20de årliga Debiankonferensen."*
säger Egbert Gracias, Senior Software Development Manager på Lenovo. *"Vi är
glada att på nära håll se det stora arbete som utförs i gemenskapen och möta
utvecklarna och volontärerna som ser till att Debianprojektet rör sig framåt!"*

Lenovo är ett globalt ledande teknologiföretag som tillverkar en bred portfölj
av anslutna produkter, inklusive smartphones, surfplattor, PCs och arbetsstationer
så väl som AR/VR-enheter, lösningar för smarta hem/kontor samt datacenterlösningar.

Med detta engagemang som Platinasponsor bidrar Lenovo till att göra vår
årliga konferens möjlig, och stöjder direkt utvecklingen av Debian och Fri mjukvara,
vilket bidrar till att stärka den gemenskap som fortsätter att samarbeta på
Debianprojektet under resten av året.

Tack så mycket Lenovo för ert stöd av DebConf19!

## Bli en sponsor du med!

DebConf19 accepterar fortfarande nya sponsorer.
Intresserade företag och organisationer kan kontakta DebConf-gruppen
genom [sponsors@debconf.org](mailto:sponsors@debconf.org), och
besöka DebConf19s webbplats på [https://debconf19.debconf.org](https://debconf19.debconf.org).
