Title: DebConf19 is looking for sponsors!
Date: 2019-01-10 18:30
Tags: debconf, debconf19, sponsors
Slug: debconf19-looking-for-sponsors
Author: Laura Arjona Reina, Andre Bianchi and Paulo Santana
Artist: DebConf19 Design Team
Status: published

**[DebConf19](https://debconf19.debconf.org) will be held in
[Curitiba](https://debconf19.debconf.org/about/curitiba/), Brazil from July 21th
to 28th, 2019**. 
It will be preceded by DebCamp, July 14th to 19th, and [Open
Day](https://debconf19.debconf.org/schedule/openday/) on the 20th.

[DebConf](https://www.debconf.org), Debian's annual developers conference,
is an amazing event where Debian contributors from all around the world
gather to present, discuss and work in teams around the Debian operating
system. It is a great opportunity to get to know people responsible for the
success of the project and to witness a respectful and functional distributed
community in action.

The DebConf team aims to organize the Debian Conference
as a self-sustaining event, despite its size and complexity.
The financial contributions and support by individuals, companies and
organizations are pivotal to our success.

There are many different possibilities to support DebConf
and we are in the process of contacting potential sponsors 
from all around the globe.
If you know any organization that could be interested
or who would like to give back resources to FOSS,
please consider handing them
the [sponsorship brochure](https://media.debconf.org/dc19/fundraising/debconf19_sponsorship_brochure_en.pdf)
or [contact the fundraising team](mailto:sponsors@debconf.org) with any leads.
If you are a company and want to sponsor, please contact us at [sponsors@debconf.org](mailto:sponsors@debconf.org).

Let’s work together, as every year, on making the best DebConf ever.
We are waiting for you at Curitiba!

![DebConf19 logo](|static|/images/800px-Debconf19-horizontal.png)
