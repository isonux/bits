Title: 新的 Debian 开发者和维护者 (2019年3月 至 4月)
Slug: new-developers-2019-04
Date: 2019-05-11 16:35
Author: Jean-Pierre Giraud
Tags: project
Lang: zh-CN
Translator: Boyuan Yang
Status: published


下列贡献者在过去的两个月中获得了他们的 Debian 开发者帐号：

  * Jean-Baptiste Favre (jbfavre)
  * Andrius Merkys (merkys)

下列贡献者在过去的两个月中被添加为 Debian 维护者：

  * Christian Ehrhardt
  * Aniol Marti
  * Utkarsh Gupta
  * Nicolas Schier
  * Stewart Ferguson
  * Hilmar Preusse

祝贺他们！

