Title: Nous desenvolupadors i mantenidors de Debian (maig i juny del 2017)
Slug: new-developers-2017-06
Date: 2017-07-02 14:30
Author: Jean-Pierre Giraud
Tags: project
Lang: ca
Status: published

Els següents col·laboradors del projecte han esdevingut Debian Developers
en els darrers dos mesos:

  * Alex Muntada (alexm)
  * Ilias Tsitsimpis (iliastsi)
  * Daniel Lenharo de Souza (lenharo)
  * Shih-Yuan Lee (fourdollars)
  * Roger Shimizu (rosh)

Els següents col·laboradors del projecte han esdevingut Debian Maintainers
en els darrers dos mesos:

  * James Valleroy
  * Ryan Tandy
  * Martin Kepplinger
  * Jean Baptiste Favre
  * Ana Cristina Custura
  * Unit 193


Enhorabona a tots!
