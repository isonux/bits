Title: DebConf17 accueille ses dix-huit premiers commanditaires !
Slug: dc17-welcome-its-first-sponsors
Date: 2017-03-20 15:15
Author: Laura Arjona Reina and Tássia Camões Araújo
Lang: fr
Translator: Émile Plourde-Lavoie
Tags: debconf17, debconf, sponsors
Status: published

![DebConf17 logo](|static|/images/800px-Dc17logo.png)

DebConf17 se tiendra à Montréal, Canada en août 2017. Nous travaillons très fort
pour mobiliser coeurs et esprits et permettre, encore une fois, que cette conférence
soit un terrain fertile pour l'épanouissement du Projet Debian. Joignez-nous et
supportez cet événement marquant du calendrier du Logiciel Libre.

Dix-huit entreprises se sont déjà engagées à commanditer DebConf17 ! Tout en les accueillant
chaleureusement, nous aimerions vous les présenter.

Notre premier commanditaire Platine est [**Savoir-faire Linux**](https://www.savoirfairelinux.com/),
une entreprise de développement de logiciel libre et ouvert basée à Montréal qui offre des
solutions d'intégration Linux et de logiciels libres et contribue activement à plusieurs
projets du libre. *"Nous croyons [que Debian] est une pièce essentielle, dans un sens
social et politique, pour la liberté des utilisateurs de systèmes technologiques modernes"*,
a affirmé Cyrille Béraud, président de Savoir-faire Linux.

Notre premier commanditaire Or est [**Valve**](http://www.valvesoftware.com/),
une entreprise développant des jeux, une plateforme de divertissement social
et des moteurs de jeu. Notre second commanditaire Or est
[**Collabora**](https://www.collabora.com/), qui offre un vaste répertoire de
services aidant ses clients à naviguer dans le monde toujours en évolution
du code ouvert.

En tant que commanditaires Argent, nous avons
[**credativ**](http://www.credativ.de/)
(une entreprise de services se concentrant sur le logiciel à code ouvert et un
[partenaire Debian](https://www.debian.org/partners/)),
[**Mojatatu Networks**](http://www.mojatatu.info/)
(une entreprise canadienne développant des solutions de _Software Defined Networking_ (SDN)),
la [**Haute école spécialisée bernoise**](https://www.bfh.ch/)
(avec plus de [6,600](https://www.bfh.ch/fr/bfh/facts_figures.html) étudiants inscrits,
située dans la capitale suisse),
[**Microsoft**](https://www.microsoft.com/)
(une entreprise d'informatique multinationale américaine),
[**Evolix**](http://evolix.ca/)
(une entreprise de services intégrés et de support en TI située à Montréal),
[**Ubuntu**](https://www.canonical.com/)
(le système d'exploitation soutenu par Canonical)
et [**Roche**](http://www.roche.com/careers)
(un fournisseur pharmaceutique international majeur et une entreprise de recherche
dédiée aux soins de santé personnalisés).

[**ISG.EE**](http://isg.ee.ethz.ch/),
[**IBM**](https://www.ibm.com/),
[**Bluemosh**](https://bluemosh.com/),
[**Univention**](https://www.univention.com/)
et [**Skroutz**](https://www.skroutz.gr/)
sont nos commanditaires Bronze jusqu'à présent.

Finalement, la [**Fondation Linux**](https://www.linuxfoundation.org/),
[**Réseau Koumbit**](https://www.koumbit.org/)
et [**adte.ca**](http://www.adte.ca/)
sont nos commanditaires supporteurs.

## Devenez aussi commanditaire !

Aimeriez-vous devenir commanditaire ? Connaissez-vous ou travaillez-vous pour une
entreprise ou une organisation qui pourrait envisager la commandite ?

Veuillez consulter notre
[brochure de commandite](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_brochure_fr.pdf)
(ou un [feuillet](https://media.debconf.org/dc17/fundraising/debconf17_sponsorship_flyer_fr.pdf) résumé),
dans lesquels sont exposés tous les détails du programme et sont décrits les
atouts d'une commandite.

Pour de plus amples détails, n'hésitez pas à nous contactez à
[sponsors@debconf.org](mailto:sponsors@debconf.org), et
visitez le site de DebConf17 : [https://debconf17.debconf.org](https://debconf17.debconf.org).
