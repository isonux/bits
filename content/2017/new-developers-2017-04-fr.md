Title: Nouveaux développeurs et mainteneurs de Debian (mars et avril 2017)
Slug: new-developers-2017-04
Date: 2017-05-15 12:30
Author: Jean-Pierre Giraud
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

  * Guilhem Moulin (guilhem)
  * Lisa Baron (jeffity)
  * Punit Agrawal (punit)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

  * Sebastien Jodogne
  * Félix Lechner
  * Uli Scholler
  * Aurélien Couderc
  * Ondřej Kobližek
  * Patricio Paez

Félicitations !
