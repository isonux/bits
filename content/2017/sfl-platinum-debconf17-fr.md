Title: Savoir-faire Linux parrain de platine pour DebConf17
Slug: sfl-platinum-debconf17
Date: 2017-01-30 17:50
Author: Laura Arjona Reina and Tássia Camões Araújo
Translator: Cédric Boutillier
Tags: debconf17, debconf, sponsors, Savoir-faire Linux
Lang: fr
Status: published

[![SFLlogo](|static|/images/savoirfairelinux.png)](https://www.savoirfairelinux.com/)

Nous sommes heureux d'annoncer que [**Savoir-faire Linux**](https://www.savoirfairelinux.com/)
soutient [DebConf17](http://debconf17.debconf.org) en tant que parrain de platine.

« Debian est un modèle à la fois pour le logiciel libre et pour les communautés
de développeurs. Savoir-faire Linux promeut non seulement la vision mais aussi
les valeurs du projet. En effet, nous croyons qu'il joue un rôle majeur, à la
fois socialement et politiquement, pour la liberté des utilisateurs de systèmes
de technologie modernes », a déclaré Cyrille Béraud, président de Savoir-faire
Linux.

Savoir-faire Linux est une entreprise de logiciel libre et au code source
ouvert basée à Montréal, avec des bureaux à Québec, Toronto, Paris et Lyon.
Elle fournit des solutions d'intégration pour Linux et le logiciel libre afin
d'offrir performance, flexibilité et indépendance à ses clients. Cette compagnie
contribue activement à de nombreux projets de logiciel libre et fournit des
miroirs pour Debian, Ubuntu, le noyau Linux et d'autres projets.

Savoir-faire Linux était présent au programme de DebConf16, avec un exposé sur
Ring, leur système de communication sécurisé et distribué sous license GPL.
Ring est entré dans la version testing pendant DebCamp in 2016, et fera partie
de la prochaine version stable Stretch.
OpenDHT, l'implémentation de table de hachage distribuée utilisée par Ring est
aussi apparue dans Debian experimental pendant la dernière DebConf.

Avec cet engagement en tant que parrain de platine,
Savoir-faire Linux contribue à rendre possible notre conférence annuelle,
et soutient directement les avancées de Debian et du logiciel libre
en aidant à renforcer la communauté qui continue à travailler ensemble
sur les differents aspects du projet Debian tout au long de l'année.

Merci beaucoup Savoir-faire Linux pour votre soutien à DebConf17 !

## Vous aussi, devenez parrain !

DebConf17 accepte encore les parrainages.
Les entreprises et organisations intéressées peuvent contacter l'équipe DebConf
par courriel à l'adresse [sponsors@debconf.org](mailto:sponsors@debconf.org), et
visiter le site Internet de
DebConf17 [http://debconf17.debconf.org](http://debconf17.debconf.org).

