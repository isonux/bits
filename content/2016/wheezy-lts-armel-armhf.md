Title: Debian 7 Wheezy LTS now  supporting armel and armhf
Slug: wheezy-now-supporting-armel-and-armhf
Date: 2016-06-02 08:00
Author: Markus Koschany
Tags: Wheezy, LTS, debian
Status: published

Debian Long Term Support (LTS) is a project created to extend the life of all Debian stable releases to (at least) 5 years.

Thanks to the LTS sponsors, Debian's buildd maintainers and the Debian FTP Team are excited to announce that two new architectures, **armel** and **armhf**, are  going to be supported in Debian 7 Wheezy LTS. These architectures along with  **i386**  and **amd64** will receive two additional years of extended security support.

Security updates for Debian LTS are not handled by the native Debian Security Team, but instead by a separate group of volunteers and companies interested in making it a success.

Wheezy's LTS period [started][1] a few weeks ago and more than thirty updates [have been announced][2] so far. If you use Debian 7 Wheezy, you do not need to change anything in your system to start receiving those updates.

More information about how to use Debian Long Term Support and other important changes regarding Wheezy LTS is available at [https://wiki.debian.org/LTS/Using][3]

[1]: https://www.debian.org/News/2016/20160425
[2]: https://lists.debian.org/debian-lts-announce/
[3]: https://wiki.debian.org/LTS/Using


