Title: Debian fa 22 anys!
Date: 2015-08-16 23:59
Tags: birthday, debian
Slug: 22-birthday-debian
Author: Ana Guerrero Lopez i Valessio Brito
Translator: Adrià García-Alzórriz
Lang: ca
Status: published

Disculpeu-nos per publicar-ho tan tard; hem estat enfeinats amb la [DebConf15](http://debconf15.debconf.org/)!


![Debian 22](|static|/images/debian22.jpg)


Feliç 22è aniversari, Debian!


