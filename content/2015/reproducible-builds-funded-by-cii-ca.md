Title: La «Core Infrastructure Initiative» finança les generacions reproduïbles
Slug: reproducible-builds-funded-by-cii
Date: 2015-06-23 14:00
Author: Anam Guerrero Lopez
Translator: Adrià García-Alzórriz, Lluís Gili
Tags: debian, cii, reproducible builds
Lang: ca
Status: published


La «Core Infrastructure Initiative» [ha anunciat][lf-announce] avui
que recolzaran dos Desenvolupadors Debian, Holger Levsen i Jérémy
Bobbio amb 200.000$ per tal que avancin amb la seva feina a Debian en
compilacions reproduïbles i per col·laborar més estretament amb altres
distribucions com Fedora, Ubuntu, OpenWrt per beneficiar-se d'aquest
esforç.

[lf-announce]:http://www.linuxfoundation.org/news-media/announcements/2015/06/linux-foundation-s-core-infrastructure-initiative-funds-three-new

La [Core Infrastructure Initiative][cii-link] (CII) es va crear el 2014
per tal de fortificar la seguretat dels projectes clau de programari
lliure. Aquesta iniciativa la van fundar més de 20 empreses i està
gestionada per «The Linux Foundation».
[cii-link]: http://www.linuxfoundation.org/programs/core-infrastructure-initiative

La iniciativa de la [generació reproduïble][rb] vol permetre que
qualsevol pugui recompilar bit a bit paquets binaris idèntics d'una
mateixa font, i per tant permeti a cadascú verificar de forma
independent que un binari coincideix amb el codi font del qual es diu
que se'n deriva. Per exemple, això permetrà que els usuaris de Debian
puguin regenerar paquets i obtenir exactament paquets idèntics que els
que es proveeixen des dels repositoris de Debian.

[rb]:https://wiki.debian.org/ReproducibleBuilds

