Title: Nouveaux développeurs et mainteneurs de Debian (juillet et août 2015)
Slug: new-developers-2015-08
Date: 2015-09-01 13:45
Author: Ana Guerrero López
Tags: project
Lang: fr
Status: published

Les contributeurs suivants sont devenus développeurs Debian ces deux
derniers mois :

  * Gianfranco Costamagna (locutusofborg)
  * Graham Inggs (ginggs)
  * Ximin Luo (infinity0)
  * Christian Kastner (ckk)
  * Tianon Gravi (tianon)
  * Iain R. Learmonth (irl)
  * Laura Arjona Reina (larjona)

et ces contributeurs ont été acceptés comme mainteneurs Debian durant la
même période :

  * Senthil Kumaran
  * Riley Baird
  * Robie Basak
  * Alex Muntada
  * Johan Van de Wauw
  * Benjamin Barenblat
  * Paul Novotny
  * Jose Luis Rivero
  * Chris Knadle
  * Lennart Weller

Félicitations !

