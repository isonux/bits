Title: Debian a 22 ans !
Date: 2015-08-16 23:59
Tags: birthday, debian
Slug: 22-birthday-debian
Author: Ana Guerrero Lopez and Valessio Brito
Translator: Jean-Pierre Giraud
Lang: fr
Status: published

Désolés de poster si tard, nous sommes très occupés à [DebConf15](http://debconf15.debconf.org/)!


![Debian 22](|static|/images/debian22.jpg)


Joyeux 22e anniversaire Debian !


