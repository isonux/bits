Title: Debian lamenta el fallecimiento de Ian Murdock
Date: 2015-12-30 20:00:00
Tags: ian murdock, debian, in memoriam
Lang: es
Slug: mourning-ian-murdock
Author: Ana Guerrero Lopez, Donald Norwood y Paul Tagliamonte
Translator: Adrià García-Alzórriz
Status: published


![Ian Murdock](|static|/images/ianmurdock.jpg)


Con el corazón compungido, Debian lamenta el fallecimiento de Ian Murdock, firme partidario del software libre y abierto, padre, hijo y el 'ian' en Debian.

Ian empezó el proyecto en agosto de 1993, publicando la primera versión
 de Debian más tarde en el mismo año. Debian se convertiría así en el
sistema operativo universal para el mundo, funcionando en cualquier
dispositivo, desde sistemas embebidos hasta la estación espacial.

Ian se centró en crear una distribución y una cultura de comunidad que hiciera lo correcto tanto en lo ético como en lo técnico. Publicar cuando esté listo y una postura firme sobre la libertad de software son las reglas de oro en el mundo del software libre y de código abierto.

La devoción le guió en su trabajo, tanto en Debian como en los años posteriores, siempre trabajando hacia un futuro lo mejor posible.

El sueño de Debian está vivo, la comunidad permanece increíblemente activa con miles de desarrolladores que trabajan incontables horas para ofrecer un sistema operativo seguro y confiable.

La comunidad de Debian da el pésame a la familia en este momento tan difícil.

Su familia nos ha pedido discreción durante estos momentos difíciles y
deseamos respetarlo. Desde dentro de Debian y de la amplia comunidad de
Linux podemos expresar nuestras muestras de condolencia a
in-memoriam-ian@debian.org, donde serán guardadas y archivadas.
