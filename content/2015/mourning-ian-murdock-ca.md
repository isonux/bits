Title: Debian lamenta el decés de l'Ian Murdock
Date: 2015-12-30 20:15:00
Tags: ian murdock, in memoriam
Slug: mourning-ian-murdock
Lang: ca
Author: Ana Guerrero Lopez, Donald Norwood i Paul Tagliamonte
Translator: Adrià García-Alzórriz, Xavier Drudis
Status: published


![Ian Murdock](|static|/images/ianmurdock.jpg)


Amb el cor compungit, Debian lamenta la pèrdua d'Ian Murdock, ferm
partidari del programari lliure i de codi obert, pare, fill i l'"ian"
a Debian.

L'Ian va començar el projecte Debian l'agost de 1993, i va publicar les
primeres versions més tard el mateix any. Debian es convertiria en el
sistema operatiu universal pel món, que funciona en tot tipus de
dispositius, des de sistemes encastats fins a l'estació especial.

L'Ian es va centrar en crear una distribució i una cultura de comunitat
que fessin el correcte, èticament o tècnica. Publicar quan estigui a
punt i la ferma posició sobre la llibertat de programari són les regles
d'or en el món del programari lliure i de codi obert.

La seva devoció per fer el correcte va guiar la seva feina, tant a
Debian com els següents anys, sempre treballà per assolir el millor
futur possible.

El somni de l'Ian està viu, la comunitat de Debian continua
increïblement activa, amb milers de desenvolupadors que treballen
innumerables hores per proporcionar un sistema operatiu segur i fiable.

Els pensaments de la comunitat de Debian són amb la família de l'Ian en
aquests moments tant durs.

La seva família ens ha demanat privacitat en aquests moments difícils i
volem respectar-ho. Des de Debian i des de la comunitat de Linux en
general podem enviar mostres de condol a in-memoriam-ian@debian.org;
seran guardades i arxivades.

