Title: DebConf15 da la bienvenida a los nuevos patrocinadores
Slug: new-sponsors-debconf15
Date: 2015-03-18 16:00
Author: Laura Arjona Reina
Translator: Daniel Cialdella
Tags: debconf15, debconf, sponsors
Lang: es
Status: draft


La organización de **DebConf15** (del 15 al 22 de Agosto de 2015, en Heidelberg,
Alemania) va sobre ruedas, la [convocatoria de propuestas está abierta](https://bits.debian.org/2015/03/debconf15-cfp.html)
y hoy queremos proveerles de novedades sobre nuestros patrocinadores.

Doce compañías más se han unido a [nuestros nueve primeros patrocinadores](https://bits.debian.org/2014/11/dc15-welcome-its-first-sponsors.html) en dar apoyo a DebConf15. ¡Gracias a todos ellos!

Nuestro tercer Patrocinador de Oro es la [**Fundación Matanel**](http://www.matanel.org/), 
que fomenta la iniciativa social en todo el mundo.

[**IBM**](http://www.ibm.com), la corporación tecnológica y consultora, también se ha unido
como Patrocinador de Oro en DebConf15.

[**Google**](http://google.com), la compañía dueña del motor de búsquedas y anuncios,
ha incrementado su nivel de Plata a Oro.

[**Mirantis**](http://www.mirantis.com/), [**1&1**](http://www.1und1.de/)
(que es también uno de los socios de servicios de Debian),
[**MySQL**](http://www.mysql.com/) y [**Hudson River Trading**](http://www.hudson-trading.com/) se han confirmado como patrocinadores de Plata.

Y en último lugar pero no menos importante, seis patrocinadores más que han aceptado darnos apoyo con nivel Bronce: [**Godiug.net**](http://www.godiug.net/),
[**La Universidad de Zúrich**](http://www.ifi.uzh.ch/),
[**Deduktiva**](http://www.deduktiva.com/),
[**Docker**](http://www.docker.com/),
[**DG-i**](http://www.dg-i.de/) (que es también uno de los socios de servicios de Debian),
y [**PricewaterhouseCoopers**](http://www.pwc.de/) (que también proporciona soporte en consultoría para DebConf15).

El equipo de la DebConf15 está más que agradecido a todos los patrocinadores de la DebConf por su apoyo.

## ¡Apúntate también para darnos tu apoyo!

DebConf15 sigue aceptando patrocinadores. Compañías y Organizaciones interesadas pueden contactar
con el equipo de la DebConf a través de  [sponsors@debconf.org](mailto:sponsors@debconf.org),
y visitar el sitio en internet de DebConf15 en [http://debconf15.debconf.org](http://debconf15.debconf.org).

