Title: DPL election is over, Lucas Nussbaum re-elected
Date: 2014-04-14 08:10
Tags: dpl
Slug: results-dpl-election-2014
Author: Ana Guerrero Lopez
Status: published

The Debian Project Leader election has concluded and the winner is Lucas Nussbaum.
Of a total of 1003 developers, 401 developers voted using the [Condorcet method](http://en.wikipedia.org/wiki/Condorcet_method).

More information about the result is available in the [Debian Project Leader Elections 2014 page](https://www.debian.org/vote/2014/vote_001).

The new term for the project leader will start on April 17th and expire on April 17th 2015.
