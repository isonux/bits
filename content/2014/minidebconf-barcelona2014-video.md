Title: Video streams for the MiniDebConf 2014 Barcelona
Date: 2014-03-15 12:10
Tags: minidebconf, debian women, announce
Slug: minidebconf-barcelona2014-video
Author: Martín Ferrari
Status: published

This is just a quick note to tell you that the video stream of the Barcelona [MiniDebConf](http://bcn2014.mini.debconf.org/) will be available at the following URL:

[http://bcn2014.video.debconf.org/](http://bcn2014.video.debconf.org/)

If you were not able to make it to Barcelona, now you can still follow from home!

May you have a productive and joyful MiniDebConf - and thanks for volunteering and talking if you do so! The MiniDebConf is what you make it.
