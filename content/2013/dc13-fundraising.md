Title: Last days for the DebConf13 matching fund!
Date: 2013-04-26 15:30
Tags: debconf13
Slug: dc13-fundraising
Author: Brian Gupta
Status: published

As part of the DebConf13 fundraising efforts, Brandorr Group
is funding a matching initiative for DebConf13, which will be
in place for 4 more days (through April 30th).

You can [donate here](http://deb.li/dc13donate)!

Please consider donating $100, or even $5 or any amount in
between, as we can use all the help we can get to reach our
fundraising target.
The rules are simple:

* for each dollar donated by an individual to DebConf13 though
  this mechanism, Brandorr Group will donate another dollar;
* individual donations will be matched only up to 100USD each;
* only donations in USD will be matched;
* Brandorr Group will match the donated funds up to a maximum
 total of 5000 USD.

This generous offer will only stay in place through the end of
April 30th.

Please act quickly, and help spread the world!
