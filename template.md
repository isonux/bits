Title: This is my awesome news item
Slug: awesome-news-item
Date: YYYY-MM-DD HH:MM
Author: Name Surname(s)
Tags: tag1, tag2
Status: draft


We're excited to announce my awesome news item about [the Debian Project](https://www.debian.org)
and for that I'm writing this blog post in Markdown.

My second parapgrah has text attributes in *italic*, **bold** and `monospace`.

And I want to add a list:

 * foo
 * bar
 * baz

This is my last paragraph.
